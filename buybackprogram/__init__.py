"""Buyback program plugin app for Alliance Auth."""

default_app_config = "buybackprogram.apps.BuybackProgramConfig"

__version__ = "1.12.1"
__title__ = "Buyback Program"
